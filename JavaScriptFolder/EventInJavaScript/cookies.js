
window.onload = function () {
    if (document.cookie.length != 0) {
        var nameValueArray = document.cookie.split("=");
        document.bgColor = nameValueArray[1];
        document.getElementById("ddlTheme").value = nameValueArray[1];
    }
}


function setColor() {
    var selectedColor = document.getElementById("ddlTheme").value;

    if (selectedColor != "select Color") {
        document.bgColor = selectedColor;
        document.cookie = "color=" + selectedColor + ";expires=Fri, 5 Aug 2018 01:00:00 UTC;";
    }
}
