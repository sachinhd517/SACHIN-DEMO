'use strict';

var cat = {
    name: {first: 'Fluffy', last: 'LaBeouf'},
    color: 'White'
}

Object.defineProperty(cat, 'name', {enumerable: false})

// for(var propertyName in cat)
// {
//     display(propertyName + ':' + cat[propertyName])
// }
// display(Object.keys(cat))

// display(JSON.stringify(cat))
display(cat['name'])