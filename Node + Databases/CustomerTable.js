var mysql = require('mysql');

var con = mysql.createConnection({
    host: 'localhost',
    user: "root",
    password: "",
    database: "javatpoint"
});

con.connect(function(error){
    if(error) throw error;
    console.log("Connected!!!!!!!");
    var sql = "INSERT INTO Customers (Id,name,address) values ? ";
    var values = [
    [2,'John', 'Highway 71'],
    [3,'Peter', 'Lowstreet 4'],
    [4,'Amy', 'Apple st 652'],
    [5,'Hannah', 'Mountain 21'],
    [6,'Michael', 'Valley 345'],
    [7,'Sandy', 'Ocean blvd 2'],
    [8,'Betty', 'Green Grass 1'],
    [9,'Richard', 'Sky st 331'],
    [10,'Susan', 'One way 98'],
    [11,'Vicky', 'Yellow Garden 2'],
    [12,'Ben', 'Park Lane 38'],
    [13,'William', 'Central st 954'],
    [14,'Chuck', 'Main Road 989'],
    [15,'Viola', 'Sideway 1633']
    ];

    con.query(sql,[values],function(error,result){
        if(error) throw error;
        console.log("Number of reroced is inserted : " + result.affectedRows);
    });
});