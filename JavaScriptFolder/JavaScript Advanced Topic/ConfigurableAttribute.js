'use strict';

var cat = {
    name: {first: 'Fluffy', last: 'LaBeouf'},
    color: 'White'
}

// Object.defineProperty(cat, 'name', {enumerable: false})
// Object.defineProperty(cat, 'name', {enumerable: true})

delete cat.name

display(cat.name)

