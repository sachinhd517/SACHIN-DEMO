'use strict';

function Cat(name,color){
    this.name = name
    this.color = color
}

Cat.prototype.age = 4

var fluffy = new Cat('Fluffy','White')
var muffin = new Cat('Muffin','Brown')

fluffy.age = 5

display(fluffy.age)
display(fluffy.__proto__.age)

// display(Object.keys(fluffy))
display(fluffy.hasOwnProperty('age'))
