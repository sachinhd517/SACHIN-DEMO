var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "javatpoint"
});

con.connect(function(err){
    if(err) throw err;
    var sql = "DROP TABLE IF EXISTS Customers";
    con.query(sql, function (err, result){
        if (err) throw err;
        console.log(result);
    }); 
});