var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "javatpoint"
});

con.connect(function(err){
    if(err) throw err;
    var sql = "insert into Users (id,name,favorite_product)values ? ";
    var values = [
         [1,  'John',  154],
         [2,  'Peter', 154],
         [3,  'Amy',  155],
         [4,  'Hannah',null],
         [5,  'Michael',null ]
        
    ];
    con.query(sql, [values], function(err, result){
        if(err) throw err;
        console.log("Number of recored is inserted!!!!!!!!!!!" + result.affectedRows);
    });
});