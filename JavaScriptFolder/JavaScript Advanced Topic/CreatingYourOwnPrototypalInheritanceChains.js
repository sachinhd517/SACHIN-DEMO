'use strict';

function Animal(voice){
    this.voice = voice || 'grunt'
}
Animal.prototype.speak = function(){
    display(this.voice)
}

function Cat(name, color){
    Animal.call(this, 'Meow')
    this.name = name
    this.color = color
}


Cat.prototype = Object.create(Animal.prototype)
Cat.prototype.constructor = Cat


var fluffy = new Cat('Fluffy','White')

display(fluffy.__proto__.__proto__)

// display(fluffy instanceof Animal)

