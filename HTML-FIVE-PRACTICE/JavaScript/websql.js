function CreateDB()
{
    var DataBaseName = 'MyDataBase';
    var Version = 1.0;
    var Text_Description = 'My First Web-SQL Example';
    var Database_Size = 2 * 1024 * 1024;
    var dbObj = openDatabase(DataBaseName,Version,Text_Description, Database_Size, OnSuccessCreate());
    dbObj.transaction(function (tx){
        tx.executeSql('CREATE TABLE IF NOT EXIT Employee_Table (id unique, Name, Location)');
        alert('Table Create Succeefully');
    });
    function OnSuccessCreate()
    {
        alert('Database Created Successfully');
    }
    
}
function InsertTable()
    {
       
        var id = document.getElementById('userid').value;
        var name = document.getElementById('username').value;
        var location = document.getElementById('userlocation').value;
        var dbObj;
         dbObj.transaction(function (tx){
            tx.executeSql('insert into Employee_Table values('+id+',"'+ name +'","'+ location +'")');
            alert('Inserted Successfully...');
        });
    }