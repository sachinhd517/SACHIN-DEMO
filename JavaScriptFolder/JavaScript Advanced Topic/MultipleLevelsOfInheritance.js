'use strict';

function Cat(name, color)
{
    this.name = name
    this.color = color
}

Cat.prototype.age = 5

var fluffy = new Cat('Fluffy', 'White')

display(fluffy.__proto__)
display(fluffy.__proto__.__proto__)
display(fluffy.__proto__.__proto__.__proto__)
